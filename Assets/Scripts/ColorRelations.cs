﻿using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    /* Clase que relaciona un color de elemento con el correspondiente código visual */
    [System.Serializable]
    public class CRelation {
        public ElementColor elementColor;
        public Color color;
        public Material neonMaterial;
    }

    [CreateAssetMenu(menuName = "Divide & Conquer/ColorRelations", order = 1)]
    public class ColorRelations : ScriptableObject {
        public List<CRelation> colors;
    }

}