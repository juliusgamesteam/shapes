﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM.DivideAndConquer {

    public class PreLevelController : MonoBehaviour {

        // Start is called before the first frame update
        void Start() {
            StartCoroutine(GameManager.PlayLevel());
        }
    }

}