﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace JCGM.DivideAndConquer {

    public class PostLevelCoinsController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al contenedor de la puntuación obtenida")]
        public GameObject scoreContainer;
        [Tooltip("Referencia al componente Text de la puntuación")]
        public Text scoreText;
        [Space]
        [Tooltip("Referencia al contenedor de las monedas obtenidas")]
        public GameObject coinsContainer;
        [Tooltip("Referencia al componente Text del contador de monedas")]
        public Text coinsCounterText;
        [Space]
        [Tooltip("Referencia al GameObject del botón para multiplicar las monedas")]
        public GameObject multiplyButton;
        [Tooltip("Referencia al GameObject del botón para continuar")]
        public GameObject nextButton;
        [Space]
        [Tooltip("Referencia al sistema de partículas de las monedas")]
        public ParticleSystem coinsPS;

        [Header("Coins Reward")]
        [Tooltip("Número base de monedas a recibir en cada partida")]
        public int initialCoins;
        [Tooltip("Puntos necesarios para obtener cada moneda")]
        public int pointsPerCoin;

        /// <summary>
        /// Número de monedas obtenidas por el jugador
        /// </summary>
        private int coinsObtained;

        /// <summary>
        /// ¿Se ha multiplicado ya el número de monedas?
        /// </summary>
        private bool coinsMultiplied;

        /// <summary>
        /// Sequence a usar por el tween
        /// </summary>
        private Sequence seq;

        #region Unity Messages

        private void Awake() {
            // Oculta los elementos para ir mostrándolos poco a poco a continuación
            scoreContainer.SetActive(false);
            coinsContainer.SetActive(false);
            multiplyButton.SetActive(false);
            nextButton.SetActive(false);
        }

        private void Start() {
            // Muestra la puntuación en el nivel y calcula las monedas que le corresponden al jugador
            scoreText.text = GameManager.LastLevelScore().ToString();
            coinsObtained = initialCoins + (GameManager.LastLevelScore() / pointsPerCoin);
            // Lanza la animación
            StartCoroutine(ShowAnimation());
        }

        #endregion

        public void Menu() {
            // Actualiza el número de monedas del jugador y muestra el menú
            GameManager.Player.ModifyCoins(coinsObtained);
            GameManager.GoToStartMenu();
            seq.Kill();
        }

        public void MultiplyCoins() {
            // Lleva a cabo la multiplicación de las monedas, deshabilitando antes el botón
            multiplyButton.GetComponent<Button>().interactable = false;
            int prevCoins = coinsObtained;
            coinsObtained *= 2;
            coinsPS.Play(true);
            coinsCounterText.DOCounter(prevCoins, coinsObtained, 3.0f).OnComplete(() => {
                // Detiene los sistemas de partículas y habilita de nuevo el botón, salvo que ya se haya hecho la multiplicación 2 veces
                coinsPS.Stop(true);
                if (coinsMultiplied) {
                    multiplyButton.SetActive(false);
                } else {
                    multiplyButton.GetComponent<Button>().interactable = true;
                }
                coinsMultiplied = true;
            });
        }

        private IEnumerator ShowAnimation() {
            // Muestra poco a poco los primeros elementos de la escena
            WaitForSeconds wfs = new WaitForSeconds(0.75f);
            yield return wfs;
            scoreContainer.SetActive(true);
            yield return wfs;
            coinsContainer.SetActive(true);
            yield return wfs;
            // Contador de monedas, con el correspondiente sistema de partículas
            coinsPS.Play(false);
            coinsCounterText.DOCounter(0, coinsObtained, 3.0f).OnComplete(() => {
                // Detiene los sistemas de partículas y muestra los botones de multiplicar y continuar
                coinsPS.Stop(true);
                multiplyButton.SetActive(true);
                multiplyButton.transform.DOPunchScale(4 * Vector3.one, 1.75f, 1, 0.5f).OnComplete(() => {
                    seq = DOTween.Sequence();
                    seq.Append(multiplyButton.transform.DOPunchScale(0.1f * Vector3.one, 1.0f, 1, 0.5f));
                    seq.Append(multiplyButton.transform.DOShakeRotation(1.0f, 10 * Vector3.forward));
                    seq.SetDelay(1.5f).SetLoops(-1).Play();
                    nextButton.SetActive(true);
                });
            });
        }
    }

}