﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JCGM {

    public class GameLauncher : MonoBehaviour {
        [Header("Scene References")]
        [Tooltip("Animación inicial de la carga")]
        public GameLauncherAnimation gameLauncherAnimation;

        [Header("Configuration")]
        [Tooltip("Escenas a cargar tras el lanzamiento")]
        public List<string> scenes;
        [Tooltip("¿Saltar animación de espera?")]
        public bool skipWait;

        private IEnumerator Start() {
            // Espera a que se carguen los managers, si procede
            if (!skipWait) {
                yield return new WaitForSeconds(Random.Range(5.0f, 7.0f));
            }
            // Detiene la animación antes de cargar las escenas
            gameLauncherAnimation.StopAnimation(1.0f);
            yield return new WaitForSeconds(1.0f);
            // Carga las escenas de lanzamiento y detiene la animación cuando estén listas
            AsyncOperation ao;
            foreach (string scene in scenes) {
                ao = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            }
        }
    }

}