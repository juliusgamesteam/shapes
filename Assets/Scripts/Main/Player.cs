﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class Player : MonoBehaviour {
        [Tooltip("Indica si se debe cargar la información o usar la predeterminada")]
        public bool loadInfo;

        // Nivel del jugador
        [SerializeField]
        private int level;
        public int Level {
            get {
                return level;
            }
        }

        // Puntos para el siguiente nivel
        [SerializeField]
        private int pointsForLevel;
        public int PointsForLevel {
            get {
                return pointsForLevel;
            }
        }

        // Monedas del jugador
        [SerializeField]
        private int coins;
        public int Coins {
            get {
                return coins;
            }
        }

        // Start is called before the first frame update
        private void Start() {
            // Carga la información del jugador, si procede
            if (loadInfo) {
                level = PlayerPrefs.GetInt("level", 1);
                pointsForLevel = PlayerPrefs.GetInt("pointsForLevel", 1);
                coins = PlayerPrefs.GetInt("coins", 50);
            }
        }

        public int ModifyCoins(int coins2Modify) {
            // Modifica el número de monedas del jugador y devuelve el número actual
            coins += coins2Modify;

            return coins;
        }

        public int GetMaxScoreForLevel(string levelID) {
            return PlayerPrefs.GetInt("score_" + levelID, 0);
        }
    }

}