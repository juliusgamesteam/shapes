﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace JCGM {

    public class GameLauncherAnimation : MonoBehaviour {
        [Header("Scene References")]
        [Tooltip("Referencia al SpriteRenderer del fondo")]
        public SpriteRenderer backgroundRenderer;
        [Tooltip("Referencia al Transform de la forma 00")]
        public Transform shape00;
        [Tooltip("Referencia al Transform de la forma 01")]
        public Transform shape01;
        [Tooltip("Referencia al Transform de la forma 10")]
        public Transform shape10;
        [Tooltip("Referencia al Transform de la forma 11")]
        public Transform shape11;

        [Header("Configuration")]
        [Tooltip("Color con el que debe terminar la animación del fondo")]
        public Color backgroundFinalColor;
        [Tooltip("Segundos que debe durar la animación del fondo")]
        public float backgroundDuration;
        [Space]
        [Tooltip("Tamaño máximo de las formas del logo")]
        public Vector3 shapeFinalSize;

        private void Awake() {
            // Se asegura que las formas del logo empiecen invisibles
            shape00.localScale = Vector3.zero;
            shape01.localScale = Vector3.zero;
            shape10.localScale = Vector3.zero;
            shape11.localScale = Vector3.zero;
        }

        private void Start() {
            // Crea un tween para empezar la animación
            backgroundRenderer.DOColor(backgroundFinalColor, backgroundDuration).OnComplete(() => {
                // Aparición de las formas del logo
                shape00.DOPunchScale(shapeFinalSize, 1.75f, 2).SetDelay(Random.Range(0.0f, 0.75f)).SetLoops(-1);
                shape01.DOPunchScale(shapeFinalSize, 1.75f, 2).SetDelay(Random.Range(0.0f, 0.75f)).SetLoops(-1);
                shape10.DOPunchScale(shapeFinalSize, 1.75f, 2).SetDelay(Random.Range(0.0f, 0.75f)).SetLoops(-1);
                shape11.DOPunchScale(shapeFinalSize, 1.75f, 2).SetDelay(Random.Range(0.0f, 0.75f)).SetLoops(-1);
            });
        }

        public void StopAnimation(float duration) {
            // Hace desaparecer las formas del logo
            shape00.GetComponent<SpriteRenderer>().DOFade(0.0f, duration);
            shape01.GetComponent<SpriteRenderer>().DOFade(0.0f, duration);
            shape10.GetComponent<SpriteRenderer>().DOFade(0.0f, duration);
            shape11.GetComponent<SpriteRenderer>().DOFade(0.0f, duration);
        }
    }

}