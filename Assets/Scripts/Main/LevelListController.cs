﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM.DivideAndConquer {

    public class LevelListController : MonoBehaviour {

        public List<LevelStructure> levelStructures;

        public List<LevelStructure> infiniteStructures;
        public List<LevelRhythm> infiniteRhythms;


        public void LoadInfiniteLevel(int structureCode) {
            // Establece el nivel a jugar en el GameManager
            GameManager.LoadLevel("");
        }

        public void LoadRandomLevel() {
            // Objetivo del nivel
            int objective = 6 + Mathf.Clamp(Random.Range(0, GameManager.Player.Level) - 3, 0, 20);
            objective *= 5;
            objective = 5;
            // Establece aleatoriamente el nivel a jugar
            LevelStructure level = levelStructures[Random.Range(0, levelStructures.Count)];
            GameManager.LoadBasicLevel(level, level.levelRhythms[Random.Range(0, level.levelRhythms.Count)], objective);
        }

    }

}