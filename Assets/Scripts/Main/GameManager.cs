﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JCGM {

    public class GameManager : MonoBehaviour {
        /* Singleton */
        private static GameManager instance;

        [Header("References")]
        [Tooltip("Referencia a la información del jugador")]
        public Player player;
        [Tooltip("Referencia a la cámara del juego")]
        public Camera gameCamera;

        [Header("Configuration")]
        [Tooltip("Nombre del nivel que se está jugando")]
        public string currentLevel;

        /// <summary>
        /// ¿Está el juego pausado? (game over, nivel completado...)
        /// </summary>
        private bool pausedGame;

        // Configuración del nivel (estructura, ritmo, objetivo...)
        private LevelStructure structure;
        private LevelRhythm rhythm;
        private int objective;

        /// <summary>
        /// ¿Se trata de un nivel básico?
        /// </summary>
        private bool basicLevel;

        /// <summary>
        /// Puntuación obtenida en el último nivel
        /// </summary>
        private int lastLevelScore;

        #region Unity Events

        private void Awake() {
            /* Singleton */
            if (instance == null) {
                instance = this;
            } else if (instance != this) {
                Destroy(this);
            }
        }

        #endregion

        #region Game Information

        public static int CurrentLevelObjective() {
            return instance.objective;
        }

        public static LevelRhythm CurrentLevelRhythm() {
            return instance.rhythm;
        }

        public static LevelStructure CurrentLevelStructure() {
            return instance.structure; 
        }

        public static bool IsGamePaused() {
            return instance.pausedGame;
        }

        public static int LastLevelScore() {
            return instance.lastLevelScore;
        }

        public static int LastLevelMaxScore() {
            return MaxScore(instance.currentLevel);
        }

        public static int MaxScore(string levelID) {
            return instance.player.GetMaxScoreForLevel(levelID);
        }

        /* Notificaciones del juego */
        public delegate void GameState();
        public static event GameState GamePaused;
        public static event GameState GameResumed;

        /* Referencias */

        public static Camera GameCamera {
            get {
                return instance.gameCamera;
            }
        }

        public static Player Player {
            get {
                return instance.player;
            }
        }

        #endregion

        #region Public Static Methods - Scenes

        public static void GameOver(int score) {
            // Comprueba si se ha mejorado la puntuación récord hasta ahora
            instance.lastLevelScore = score;
            if (score > MaxScore(instance.currentLevel)) {
                PlayerPrefs.SetInt("score_" + instance.currentLevel, score);
            }
            // Detiene el juego y carga la escena de final de juego
            instance.GamePause(true);
            SceneManager.LoadSceneAsync("GameOver", LoadSceneMode.Additive);
        }

        public static void GoToStartMenu() {
            // Descarga la escena posterior al nivel para cargar la previa
            SceneManager.LoadSceneAsync("StartMenu", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("PostLevel");
        }

        public static void LevelCompleted(int shapes) {
            // Almacena la puntuación obtenida
            instance.lastLevelScore = shapes;
            // Detiene el juego y carga la escena de nivel completado
            instance.GamePause(true);
            SceneManager.LoadSceneAsync("LevelCompleted", LoadSceneMode.Additive);
        }

        public static void LoadBasicLevel(LevelStructure structure, LevelRhythm rhythm, int objective) {
            // Almacena el nivel a jugar y carga la escena previa, descargando el menú principal
            instance.structure = structure;
            instance.rhythm = rhythm;
            instance.objective = objective;
            instance.basicLevel = true;
            SceneManager.LoadSceneAsync("PreLevel", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("StartMenu");
        }

        public static void LoadLevel(string level) {
            // Almacena el nivel a jugar y carga la escena previa, descargando el menú principal
            instance.currentLevel = level;
            instance.basicLevel = false;
            SceneManager.LoadSceneAsync("PreLevel", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("StartMenu");
        }

        public static IEnumerator PlayLevel() {
            // Descarga la escena previa al nivel e inicia la del nivel correspondiente, seleccionándola como principal
            AsyncOperation ao = SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
            while (!ao.isDone) {
                yield return null;
            }
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level"));
            SceneManager.UnloadSceneAsync("PreLevel");
        }

        public static void PostLevel(bool gameover) {
            // Carga la escena de PostLevel, descargando la del nivel jugado y la de Game Over/Nivel completado
            SceneManager.LoadSceneAsync("PostLevel", LoadSceneMode.Additive);
            if (gameover) {
                SceneManager.UnloadSceneAsync("GameOver");
            } else {
                SceneManager.UnloadSceneAsync("LevelCompleted");
            }
            SceneManager.UnloadSceneAsync("Level");
            // Reactiva el tiempo de juego
            instance.GamePause(false);
        }

        public static void RetryLevel() {
            // Descarga la escena posterior al nivel para cargar la previa
            SceneManager.LoadSceneAsync("PreLevel", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("PostLevel");
        }

        public static void UpdateScore(int newScore) {
            // Almacena la nueva puntuación
            instance.lastLevelScore = newScore;
        }

        #endregion

        private void GamePause(bool pause) {
            // Establece la pausa del juego y lanza la notificación
            pausedGame = pause;
            if (pausedGame) {
                if (GamePaused != null) {
                    GamePaused();
                }
            } else {
                if (GameResumed != null) {
                    GameResumed();
                }
            }
        }
    }

}