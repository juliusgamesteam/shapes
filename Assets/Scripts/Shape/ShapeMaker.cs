﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class ShapeMaker : MonoBehaviour {

        [Header("Assets References")]
        [Tooltip("Referencia al prefab del personaje")]
        public GameObject characterPrefab;
        [Space]
        [Tooltip("Listado con los colores a usar")]
        public ColorRelations colorRelations;

        [Header("Configuration")]
        public Vector2 minShapeDirection;
        public Vector2 maxShapeDirection;

        // Listado con referencias a personajes sin emplear
        public static List<GameObject> UnusedShapes;

        #region Unity Messages

        private void OnDrawGizmos() {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position + Vector3.zero, transform.position + new Vector3(minShapeDirection.x, minShapeDirection.y, 0));
            Gizmos.DrawLine(transform.position + Vector3.zero, transform.position + new Vector3(maxShapeDirection.x, maxShapeDirection.y, 0));
        }

        #endregion

        #region Public Messages

        public void MakeCharacter() {
            // Elección del personaje a activar
            GameObject character;
            if (UnusedShapes.Count == 0) {
                character = GameObject.Instantiate(characterPrefab, transform.position, Quaternion.identity);
            } else {
                character = UnusedShapes[0];
                UnusedShapes.RemoveAt(0);
                character.transform.position = transform.position;
            }
            // Activa el personaje elegido, calculando su impulso inicial y asignando el color correspondiente
            Vector2 force = 600 * new Vector2(Random.Range(minShapeDirection.x, maxShapeDirection.x), Random.Range(minShapeDirection.y, maxShapeDirection.y)).normalized;
            while (force.magnitude < 200) {
                force = 600 * new Vector2(Random.Range(minShapeDirection.x, maxShapeDirection.x), Random.Range(minShapeDirection.y, maxShapeDirection.y)).normalized;
            }
            character.GetComponent<ShapeController>().SetReady(colorRelations.colors[Random.Range(0, colorRelations.colors.Count)], Random.Range(0.1f, 0.9f), force);
        }

        #endregion

    }

}