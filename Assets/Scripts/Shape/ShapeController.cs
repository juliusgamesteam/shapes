﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class ShapeController : MonoBehaviour {

        [Header("References")]
        [Tooltip("Referencia al sprite")]
        public SpriteRenderer sprite;
        [Tooltip("Referencia al rigidbody")]
        public Rigidbody2D rb;
        [Space]
        [Tooltip("Referencia al contador de colisiones")]
        public ShapeCounter counter;
        [Tooltip("Referencias a los colliders de la forma")]
        public List<Collider2D> colliders;
        [Space]
        [Tooltip("Referencia a la estela que deja al seleccionarse")]
        public TrailRenderer trail;
        [Tooltip("Referencia al sistema de partículas al seleccionarse")]
        public ParticleSystem selectedPS;
        [Tooltip("Referencia al sistema de partículas al clasificarse")]
        public ParticleSystem collectedPS;

        [Header("Configuration")]
        [Tooltip("Color del personaje")]
        public ElementColor characterColor;

        // Control de colisiones
        private int collisionCounter;
        private float lastCollision;

        // Profundidad aleatoria para evitar solapamientos
        private float randomDepth;

        // Referencia al GameplayController
        private GameplayController gameplayController;

        public bool Visible {
            get {
                return sprite.gameObject.activeInHierarchy;
            }
        }

        #region Unity Messages

        private void Awake() {
            // Busca la referencia al controlador del gameplay
            gameplayController = GameObject.FindObjectOfType<GameplayController>();
        }

        private void OnCollisionEnter2D(Collision2D other) {
            // Comprueba si debe contabilizarse la colisión
            if (Time.time - lastCollision > 0.5f) {
                // Disminuye el número de colisiones, comprobando si es el fin del nivel
                SetCollisionCounter(collisionCounter - 1);
                if (collisionCounter == 0) {
                    gameplayController.GameOver();
                }
            }
        }

        private void OnDestroy() {
            // Se asegura de no quedar notificado de la pausa
            GameManager.GamePaused -= GamePaused;
        }

        #endregion

        #region Public Messages

        public IEnumerator Collected() {
            // Desactiva la imagen de la forma, ejecuta las partículas y espera un tiempo aproximado a que desaparezcan los efectos
            sprite.gameObject.SetActive(false);
            collectedPS.Play();
            yield return new WaitForSeconds(2.5f);
            // Reactiva la imagen, desactivando por completo la forma
            sprite.gameObject.SetActive(true);
            gameObject.SetActive(false);
            // Notificación de pausa
            GameManager.GamePaused -= GamePaused;
        }

        public int GetRemainingCollisions() {
            return collisionCounter;
        }

        public void FollowPosition(Vector3 position) {
            position.z = transform.position.z;
            transform.position = position;
        }

        public void GotCaught() {
            // Desactiva sus colliders, activando la estela y el sistema de partículas correspondiente
            foreach (Collider2D col in colliders) {
                col.enabled = false;
            }
            trail.emitting = true;
            selectedPS.Play();
        }

        public void GotFree() {
            // Activa de nuevo su collider, desactivando la estela y el sistema de partículas correspondiente
            lastCollision = Time.time;
            foreach (Collider2D col in colliders) {
                col.enabled = true;
            }
            trail.emitting = false;
            selectedPS.Stop();
        }

        public void SetReady(CRelation cr, float rnd, Vector2 force) {
            // Asigna el color y material especificados a la forma
            characterColor = cr.elementColor;
            sprite.material = cr.neonMaterial;
            sprite.color = cr.color;
            counter.SetColor(cr);
            // Asigna el color y material especificados a los efectos (estela + partículas)
            trail.material = cr.neonMaterial;
            ParticleSystem.MainModule mm = selectedPS.main;
            mm.startColor = cr.color;
            mm = collectedPS.main;
            mm.startColor = cr.color;
            // Profundidad aleatoria
            randomDepth = rnd;
            // Activa la forma, inicializando el contador y aplicando el impulso inicial
            gameObject.SetActive(true);
            SetCollisionCounter(10);
            rb.AddForce(force);
            // Notificación de pausa
            GameManager.GamePaused += GamePaused;
        }

        #endregion

        private void GamePaused() {
            // Congela las físicas para que la forma se quede inmóvil
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
        }

        private void SetCollisionCounter(int cnt) {
            // Actualiza el contador
            collisionCounter = cnt;
            counter.UpdateCounter(collisionCounter);
            // Actualiza la profundidad
            Vector3 pos = transform.position;
            pos.z = (cnt - 10) - randomDepth;
            transform.position = pos;
            // Momento de la colisión
            lastCollision = Time.time;
        }

    }

}