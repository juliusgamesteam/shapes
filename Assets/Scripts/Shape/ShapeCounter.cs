﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

public class ShapeCounter : MonoBehaviour {
    [Header("References")]
    [Tooltip("Referencia al SpriteRenderer con el que modificar la transparencia de colisiones")]
    public SpriteRenderer sprite;

    #region Public Messages

    public void SetColor(CRelation cr) {
        sprite.color = cr.color;
        sprite.material = cr.neonMaterial;
    }

    public void UpdateCounter(int cnt) {
        // Actualiza la transparencia del sprite
        Color aux = sprite.color;
        aux.a = (float) (10.0f - cnt) / 10.0f;
        sprite.color = aux;
    }

    #endregion
}

}