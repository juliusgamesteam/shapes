﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class ShapeMakersManager : MonoBehaviour {

        [Header("Assets References")]
        [Tooltip("Prefab del productor de formas")]
        public GameObject shapeMakerPrefab;

        /// <summary>
        /// Referencias a los instanciadores de formas en la escena
        /// </summary>
        public List<ShapeMaker> shapeMakers;

        // Gestión de la progresión entre instancias de los personajes
        private int progressRound;
        private int roundShapesToInstance;
        private float roundInstanceTime;
        private int shapesThisTime;
        private float waitedTime;

        private LevelRhythm levelRhythm;

        #region Unity Messages

        private void Awake() {
            // Carga el ritmo del nivel
            levelRhythm.Load();
        }

        private void Start() {
            // Inicialización de variables
            progressRound = 0;
            shapesThisTime = 0;
            waitedTime = 0.0f;
            ShapeMaker.UnusedShapes = new List<GameObject>();
            // Ronda inicial
            NewProgressRound();
        }

        private void Update() {
            // Comprueba si se ha superado el tiempo de espera correspondiente
            waitedTime += Time.deltaTime;
            if ((!GameManager.IsGamePaused()) && (waitedTime >= roundInstanceTime)) {
                // Comprueba qué generador debe producir la forma
                for (int i = 0; i < shapeMakers.Count; i++) {
                    for (int j = 0; j < levelRhythm.GetSimultaneousShapesToMakeForRound(i, progressRound); j++) {
                        shapeMakers[i].MakeCharacter();
                    }
                }
                UpdateInstanceTime();
            }
        }

        #endregion

        #region Public Messages

        public void Prepare(List<Vector3> makersPositions, LevelRhythm rhythm) {
            // Instanciación de los productores y ritmo del nivel
            GameObject sm;
            shapeMakers = new List<ShapeMaker>();
            foreach (Vector3 pos in makersPositions) {
                sm = Instantiate(shapeMakerPrefab, pos, Quaternion.identity, transform);
                shapeMakers.Add(sm.GetComponent<ShapeMaker>());
            }
            levelRhythm = rhythm;
            // Activación de los productores
            gameObject.SetActive(true);
        }

        public bool ShapeCollected(ShapeController shape) {
            // Comprueba si la forma ya estaba desactivada
            bool res = shape.Visible;
            if (res) {
                StartCoroutine(ShapeDeactivation(shape));
            }

            return res;
        }

        #endregion

        private void NewProgressRound() {
            // Obtiene la progresión para la nueva ronda
            ++progressRound;
            roundShapesToInstance = levelRhythm.GetShapesToInstanceForRound(progressRound);
            roundInstanceTime = levelRhythm.GetInstanceTimeForRound(progressRound);
        }

        private IEnumerator ShapeDeactivation(ShapeController shape) {
            // Desactiva la forma y, cuando lo haya hecho por completo, la añade al listado de formas sin usar
            yield return shape.Collected();
            ShapeMaker.UnusedShapes.Add(shape.gameObject);
        }

        private void UpdateInstanceTime() {
            // Reinicia el tiempo esperado y reduce el tiempo de espera, si procede
            waitedTime = 0;
            ++shapesThisTime;
            if (shapesThisTime == roundShapesToInstance) {
                // Nueva ronda de progresión
                NewProgressRound();
                shapesThisTime = 0;
            }
        }
    }

}