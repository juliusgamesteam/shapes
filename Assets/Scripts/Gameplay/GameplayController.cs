﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class GameplayController : MonoBehaviour {

        [Header("Scene References")]
        [Tooltip("Referencia al InputManager del gameplay")]
        public GameplayInputManager inputManager;
        [Tooltip("Referencia al UIManager del gameplay")]
        public GameplayUIManager UIManager;
        [Space]
        [Tooltip("Referencia al generador de formas")]
        public ShapeMakersManager shapeMakersManager;

        // Personaje tocado por el jugador
        private ShapeController touchedShape;

        // Contadores de la partida
        private int shapesCounter;
        private int shapesObjective;

        #region Unity Messages

        private void Awake() {
            // Preparación del nivel
            shapeMakersManager.Prepare(GameManager.CurrentLevelStructure().makersPositions, GameManager.CurrentLevelRhythm()); ;
        }

        private void Start() {
            // Inicialización de variables
            shapesCounter = 0;
            shapesObjective = GameManager.CurrentLevelObjective();
            UIManager.SetShapesCounter(shapesCounter, shapesObjective);
            // Notificaciones del InputManager
            inputManager.ScreenTouchStarted += ScreenTouchStarted;
        }

        #endregion

        #region Public Messages

        public void CharacterCollected(ShapeController shape) {
            // Notificación a la forma
            if (shapeMakersManager.ShapeCollected(shape)) {
                // Actualización de contadores
                ++shapesCounter;
                UIManager.SetShapesCounter(shapesCounter, shapesObjective);
                // ¿Finalización del nivel?
                if (shapesCounter == shapesObjective) {
                    GameManager.LevelCompleted(shapesCounter);
                }
            }
        }

        public void GameOver() {
            // Comunica el fin de la partida al gestor principal, si procede
            if (!GameManager.IsGamePaused()) {
                GameManager.GameOver(shapesCounter);
            }
        }

        #endregion

        private void ScreenTouchEnded(int id, Vector2 touchPosition) {
            // Comprueba que realmente hay un personaje tocado antes de notificarle que ya es libre
            if (touchedShape != null) {
                touchedShape.GotFree();
                touchedShape = null;
                // Actualiza las notificaciones del InputManager
                inputManager.ScreenTouchStarted += ScreenTouchStarted;
                inputManager.ScreenTouchMoved -= ScreenTouchMoved;
                inputManager.ScreenTouchEnded -= ScreenTouchEnded;
            }
        }

        private void ScreenTouchMoved(int id, Vector2 touchPosition) {
            // Comprueba que realmente hay un personaje tocado antes de notificarle el movimiento
            if (touchedShape != null) {
                touchedShape.FollowPosition(GameManager.GameCamera.ScreenToWorldPoint(touchPosition));
            }
        }

        private void ScreenTouchStarted(int id, Vector2 touchPosition) {
            // Comprueba que no haya otra forma seleccionada o que el juego esté detenido
            if ((touchedShape == null) && (!GameManager.IsGamePaused())) {
                // Se comprueba si el toque es sobre una de las formas
                RaycastHit2D hit = Physics2D.Raycast(GameManager.GameCamera.ScreenToWorldPoint(touchPosition), Vector2.zero, 20f, LayerMask.GetMask("ShapeSelection"));
                if (hit) {
                    // Informa al personaje y actualiza las notificaciones del InputManager
                    touchedShape = hit.collider.GetComponentInParent<ShapeController>();
                    touchedShape.GotCaught();
                    inputManager.ScreenTouchStarted -= ScreenTouchStarted;
                    inputManager.ScreenTouchMoved += ScreenTouchMoved;
                    inputManager.ScreenTouchEnded += ScreenTouchEnded;
                }
            }
        }

    }

}