﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JCGM {

    public class GameOverController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al componente Text del marcador de este nivel")]
        public Text lastScoreText;
        [Tooltip("Referencia al componente Text del marcador récord")]
        public Text recordScoreText;

        private void Start() {
            // Establece los marcadores del jugador
            lastScoreText.text = GameManager.LastLevelScore().ToString();
            recordScoreText.text = GameManager.LastLevelMaxScore().ToString();
        }

        public void Next() {
            GameManager.PostLevel(true);
        }
    }

}