﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace JCGM {

    public class LevelCompletedController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al componente Image del fondo de la escena")]
        public Image backgroundImage;
        [Space]
        [Tooltip("Referencia al RectTransform del título del menú")]
        public RectTransform titleRectTrf;
        [Space]
        [Tooltip("Referencia al GameObject del encabezado del marcador de formas")]
        public GameObject shapesCounterHeaderText;
        [Tooltip("Referencia al componente Text del marcador de formas")]
        public Text shapesCounterText;
        [Space]
        [Tooltip("Referencia al GameObject del encabezado del marcador de puntos")]
        public GameObject levelScoreHeaderText;
        [Tooltip("Referencia al componente Text del marcador de puntos")]
        public Text levelScoreText;
        [Space]
        [Tooltip("Referencia al GameObject del botón para continuar")]
        public GameObject nextButton;
        [Space]
        [Tooltip("Referencias a los sistemas de partículas")]
        public List<GameObject> particleSystems;

        /// <summary>
        /// Puntuación final del jugador
        /// </summary>
        private int finalScore;

        /// <summary>
        /// Sequence a usar por el tween
        /// </summary>
        private Sequence seq;

        #region Unity Messages

        private void Awake() {
            // Oculta los marcadores para ir mostrándolos con la animación correspondiente
            titleRectTrf.gameObject.SetActive(false);
            shapesCounterHeaderText.SetActive(false);
            shapesCounterText.gameObject.SetActive(false);
            levelScoreHeaderText.SetActive(false);
            levelScoreText.gameObject.SetActive(false);
            nextButton.SetActive(false);
        }

        private void Start() {
            // Transparencia del fondo
            backgroundImage.DOFade(0.4f, 0.5f).OnComplete(() => {
                // Animación de las partículas y de la cabecera principal
                StartCoroutine(ParticleSystemsAnimation());
                titleRectTrf.gameObject.SetActive(true);
                titleRectTrf.DOPunchScale(4 * Vector3.one, 1.75f, 1, 0.5f).OnComplete(() => {
                    seq = DOTween.Sequence();
                    seq.Append(titleRectTrf.DOPunchScale(0.1f * Vector3.one, 1.0f, 1, 0.5f)).SetDelay(2.0f).SetLoops(-1).Play();
                    StartCoroutine(SceneAnimation());
                });
            });
            // Establece los marcadores del jugador, con la animación correspondiente
            shapesCounterText.text = GameManager.LastLevelScore().ToString();
        }

        #endregion

        public void Next() {
            // Almacena la puntuación del jugador y pasa a la siguiente escena
            GameManager.UpdateScore(finalScore);
            GameManager.PostLevel(false);
            seq.Kill();
        }

        private IEnumerator ParticleSystemsAnimation() {
            // Lleva a cabo la animación de los sistemas de partículas
            WaitForSeconds wfs = new WaitForSeconds(0.5f);
            List<GameObject> auxList = new List<GameObject>();
            auxList.AddRange(particleSystems);
            int index;
            for (int i=0; i<particleSystems.Count; i++) {
                // Activa aleatoriamente uno de los sistemas aún desactivados
                index = Random.Range(0, auxList.Count);
                auxList[index].SetActive(true);
                auxList.RemoveAt(index);
                yield return wfs;
            }
        }

        private IEnumerator SceneAnimation() {
            // Muestra poco a poco los distintos marcadores y textos
            WaitForSeconds wfs = new WaitForSeconds(0.75f);
            shapesCounterHeaderText.SetActive(true);
            yield return wfs;
            shapesCounterText.gameObject.SetActive(true);
            yield return wfs;
            levelScoreHeaderText.SetActive(true);
            yield return wfs;
            levelScoreText.gameObject.SetActive(true);
            yield return wfs;
            // Animación del marcador, mostrando el botón para continuar al finalizar
            finalScore = 100 + (10 * GameManager.LastLevelScore());
            levelScoreText.DOCounter(100, finalScore, 4).OnComplete(() => {
                nextButton.SetActive(true);
                nextButton.transform.DOPunchScale(1.5f * nextButton.transform.localScale, 0.75f, 2, 0.5f);
            });
        }
    }

}