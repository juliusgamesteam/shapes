﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

public class GameplayInputManager : MonoBehaviour {
    /* Delegados para los toques en pantalla */
    public delegate void ScreenTouchState(int id, Vector2 position);
    public ScreenTouchState ScreenTouchStarted;
    public ScreenTouchState ScreenTouchMoved;
    public ScreenTouchState ScreenTouchEnded;

#if UNITY_EDITOR

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            // Click en pantalla
            if (ScreenTouchStarted != null) {
                ScreenTouchStarted(0, Input.mousePosition);
            }
        } else if (Input.GetMouseButton(0)) {
            // Se mantiene click
            if (ScreenTouchMoved != null) {
                ScreenTouchMoved(0, Input.mousePosition);
            }
        } else if (Input.GetMouseButtonUp(0)) {
            // Se termina el click en pantalla
            if (ScreenTouchEnded != null) {
                ScreenTouchEnded(0, Input.mousePosition);
            }

        }
    }

#else

    private void Update() {
        // Comprueba si hay toques en pantalla
        if (Input.touchCount > 0) {
            // Notificación de cada toque
            foreach (Touch t in Input.touches) {
                switch (t.phase) {
                    case TouchPhase.Began:
                        // Toque en pantalla
                        if (ScreenTouchStarted != null) {
                            ScreenTouchStarted(t.fingerId, t.position);
                        }
                        break;
                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        // Se mantiene el toque
                        if (ScreenTouchMoved != null) {
                            ScreenTouchMoved(t.fingerId, t.position);
                        }
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        // Se termina el toque en pantalla
                        if (ScreenTouchEnded != null) {
                            ScreenTouchEnded(t.fingerId, t.position);
                        }
                        break;

                }
            }
        }
    }

#endif
    
}

}