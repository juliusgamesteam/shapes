﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JCGM {

    public class GameplayUIManager : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al componente Text del contador de personajes recolectados")]
        public Text charactersCounterText;
        [Tooltip("Referencia al componente Text del contador de puntos obtenidos")]
        public Text pointsCounterText;

        #region Public Messages

        public void SetPointsCounter(int points) {
            // Establece el contador de puntos
            pointsCounterText.text = points.ToString("00000");
        }

        public void SetShapesCounter(int characters, int objective) {
            // Establece el contador de formas
            charactersCounterText.text = characters.ToString("00");
            if (objective > 0) {
                charactersCounterText.text += ("/" + objective);
            }
        }

        #endregion
    }

}