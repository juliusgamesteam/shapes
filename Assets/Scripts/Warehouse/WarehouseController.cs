﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM.DivideAndConquer {

public class WarehouseController : MonoBehaviour {
    [Header("Scene References")]
    [Tooltip("Referencia al GameplayController")]
    public GameplayController gameplayController;

    [Header("Configuration")]
    [Tooltip("Color del almacén")]
    public ElementColor color;
    
    #region Unity Messages

    private void OnTriggerEnter2D(Collider2D other) {
        // Comprueba que sea un personaje
        if (other.gameObject.CompareTag("Character")) {
            // Comprueba si el color del personaje corresponde con el del almacén
            ShapeController character = other.transform.parent.parent.parent.GetComponent<ShapeController>();
            if (color.Equals(character.characterColor)) {
                // Mismo color
                gameplayController.CharacterCollected(character);
            } else {
                // Color distinto => GAME OVER
                gameplayController.GameOver();
            }
        }
    }

    #endregion
}

}