﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shapes/Level Objective")]
public class LevelObjective : ScriptableObject {
    [Tooltip("Número total de ")]
    public string progressSheetID;

    /// <summary>
    /// Referencia a la hoja de progresión
    /// </summary>
    private ES3Spreadsheet progressSheet;

    public float GetInstanceTimeForRound(int round) {
        return progressSheet.GetCell<float>(1, round);
    }

    public int GetShapesToInstanceForRound(int round) {
        return progressSheet.GetCell<int>(0, round);
    }

    public int GetSimultaneousShapesToMakeForRound(int maker, int round) {
        return progressSheet.GetCell<int>(maker + 2, round);
    }

    public void Load() {
        progressSheet = new ES3Spreadsheet();
        progressSheet.Load("progress/" + progressSheetID + ".csv");
    }
}