﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    [System.Serializable]
    public class WarehouseTranform {
        /// <summary>
        /// Posición del almacén
        /// </summary>
        public Vector3 position;

        /// <summary>
        /// Rotación del almacén
        /// </summary>
        public Quaternion rotation;

        /// <summary>
        /// Escala del almacén
        /// </summary>
        public Vector3 scale;
    }


    [CreateAssetMenu(menuName = "Shapes/Level Structure")]
    public class LevelStructure : ScriptableObject {
        [Tooltip("Listado con la información útil del Transform de los almacenes")]
        public List<WarehouseTranform> warehouses;

        [Tooltip("Listado con las posiciones de los productores de formas")]
        public List<Vector3> makersPositions;

        [Tooltip("Listado de posibles ritmos para el nivel")]
        public List<LevelRhythm> levelRhythms;
    }

}