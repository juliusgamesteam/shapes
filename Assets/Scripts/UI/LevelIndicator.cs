﻿using UnityEngine;
using UnityEngine.UI;

public class LevelIndicator : MonoBehaviour {
    public Text currentLevel;
    public Text nextLevel;

    public Image pointsForNextLevel;

    #region Public Messages

    public void Configurate(int level, int points) {
        currentLevel.text = level.ToString();
        nextLevel.text = (level + 1).ToString();
        Debug.Log((float)points / ((level + 1) * 100));
        pointsForNextLevel.fillAmount = (float) points / ((level + 1) * 100);
    }

    #endregion
}