﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCGM {

    public class StartMenuController : MonoBehaviour {
        public LevelIndicator levelIndicator;

        public GameObject gameTitle;

        public GameObject buttonPlay;
        public GameObject buttonInfinite;
        public GameObject buttonSettings;

        // Start is called before the first frame update
        private void Start() {
            levelIndicator.Configurate(GameManager.Player.Level, GameManager.Player.PointsForLevel);
        }
    }

}